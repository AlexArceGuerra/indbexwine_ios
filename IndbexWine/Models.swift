//
//  Models.swift
//  IndbexWine
//
//  Created by Mac-27 on 10/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import Foundation


struct StatusBodega{
    var title : String
    var percent : String
    init(title : String, percent : String) {
        self.title = title
        self.percent = percent
    }
}
var statusBodegaArray :[StatusBodega] = []


struct AuditoriaBodegaGroup {
    var id : String
    var title : String
    var percent: String
    var orden: Int!
    init(id : String, title : String, percent : String, orden : String) {
        self.id = id
        self.title = title
        self.percent = percent
        self.orden = Int(orden)
        
    }
}
    var auditoriaBodegaGroupArray: [AuditoriaBodegaGroup] = []


struct DetailGroup {
    var ordenFromGroup : Int!
    var orden : Int!
    var title : String
    var list: String
    var value: String
    var weight: Double!
    init(ordenFromGroup : String, orden : String, title : String, list : String, value : String, weight: String) {
        self.ordenFromGroup = Int(ordenFromGroup)
        self.orden = Int(orden)
        self.title = title
        self.list = list
        self.value = value
        self.weight = Double(weight)
            }
}
var detailGroupArray : [DetailGroup] = []

