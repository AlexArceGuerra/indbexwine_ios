//
//  ViewController.swift
//  IndbexWine
//
//  Created by Mac-27 on 4/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var shape: UIView!
    
    let imageArray = [UIImage(named: "vino")!, UIImage(named: "vendimia")!, UIImage(named: "bodega")!]
    var index : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        index = 0
        shape.alpha = 0
        backgraoundAction(self)
           self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    @objc func rotated() {
        let currentOrientation = UIDevice.current.orientation
        if (currentOrientation == UIDeviceOrientation.portrait ){
           backgroundImage.contentMode = .scaleAspectFill
        }else{
            backgroundImage.contentMode = .scaleToFill
        }
    }

    
    
     private func backgraoundAction(_ sender: Any) {
        
        Timer.scheduledTimer(timeInterval: 3,
                             target: self,
                             selector: #selector(self.animation),
                             userInfo: nil,
                             repeats: true)
    }
    
    @objc private func animation(){
        
        let toImage = imageArray[index]
        // toImage = UIImage(named:"vino")
        UIView.transition(with: self.backgroundImage,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.backgroundImage.image = toImage
        },
                          completion: nil)
        index = index + 1
        if (index == imageArray.count) { index = 0}
    }
    
}

