//
//  StatusBodegaCollectionViewCell.swift
//  IndbexWine
//
//  Created by Mac-27 on 6/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class StatusBodegaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    var percent :String!
    var title :String!
    
    

    public func update(statusBodega: StatusBodega){
        
        // test
        //percent = "0"
        percent = statusBodega.percent
        titleLabel.text = statusBodega.title
       
      
        
        
        //self.layer.removeAllSubLayers()
//
//        for subLayer in self.layer.sublayers! {
//            subLayer.removeFromSuperlayer()
//        }
        
        
        
        
        // set circle
        var  adpFrame =  circleView.frame
        adpFrame.size.width = self.frame.size.width
        adpFrame.size.height = self.frame.size.width
        circleView.frame = adpFrame
        drawCircleInView(view: self.circleView, progress: Double(percent)!)
        
        // set percentLabel
        percentLabel.font = percentLabel.font.withSize( adpFrame.size.width / 3.5)
        percentLabel.text = percent + "%"
      
        
        
    
        percentLabel.textColor = UIColor (cgColor: statusColor(progress:Double(percent)))
        
        
        // set titleLabel
         titleLabel.font = titleLabel.font.withSize(adpFrame.size.width / 7)
            
        
        
        
    }
    
    
   
        
    private func drawCircleInView(view: UIView, progress: Double){

            drawCircle(arcCenter: view.center, radius: view.frame.size.height/2, startAngle:  CGFloat(0), progress: progress)
        
    }

    private func drawCircle(arcCenter: CGPoint, radius: CGFloat, startAngle: CGFloat, progress:Double) {
        
        let myLine : CGFloat = (radius / 5)
        let myRadius = radius - myLine + 1

        
        //base
        let circlePathResto = UIBezierPath(arcCenter: arcCenter, radius: myRadius, startAngle: CGFloat(0), endAngle:  CGFloat(Double.pi * 2 ), clockwise: true)
        
        
        let shapeLayerResto = CAShapeLayer()
        shapeLayerResto.path = circlePathResto.cgPath
        
        //change the fill color
        shapeLayerResto.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayerResto.strokeColor = UIColor.darkGray.cgColor
        
        
        //you can change the line width
        shapeLayerResto.lineWidth = myLine
        
        
        // delete ant laters same type
        for subLayer in self.layer.sublayers! {
            if (subLayer.name == "shapeLayerResto"){
                subLayer.removeFromSuperlayer()}
        }
        
        // add layer
        shapeLayerResto.name = "shapeLayerResto"
        self.layer.addSublayer(shapeLayerResto)
        //view.layer.addSublayer(shapeLayerResto)
        
        //return
        
        
        let endAngle : CGFloat = CGFloat(Double.pi * 2 * progress / 100)
        let circlePath = UIBezierPath(arcCenter: arcCenter, radius: myRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //change the stroke color
        shapeLayer.strokeColor = statusColor(progress: progress)
        //change the line width
        shapeLayer.lineWidth = myLine
        
        // delete ant laters same type
        for subLayer in self.layer.sublayers! {
            if (subLayer.name == "shapeLayer"){
                subLayer.removeFromSuperlayer()}
        }
        // add layer

        shapeLayer.name = "shapeLayer"
        self.layer.addSublayer(shapeLayer)
        
        
        
        
        
    }
    

    
    
}
