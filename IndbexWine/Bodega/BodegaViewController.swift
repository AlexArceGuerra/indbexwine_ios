//
//  BodegaViewController.swift
//  IndbexWine
//
//  Created by Alejandro Arce on 13/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit


class BodegaViewController: UIViewController {

    @IBOutlet weak var detailContainerView: UIView!
    @IBOutlet weak var shape: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailContainerView.alpha = 0
        shape.alpha = 0
        
        NotificationCenter.default.addObserver(self, selector: #selector(showDetailAuditoriaAction(notification:)), name: .showDetailAuditoria, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideDetailAuditoriaAction(notification:)), name: .hideDetailAuditoria, object: nil)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    
        
        

    }
    
    @IBAction func backAction(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
        
    }
    
 
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
// Notification
    @objc func showDetailAuditoriaAction(notification: NSNotification) {
  
//        let detVC : DetailGroupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as! DetailGroupViewController
//       // detVC.view.backgroundColor = UIColor(displayP3Red: 102/255, green: 102/255, blue:102/255 , alpha: 1)
//
//        let color : CGFloat = 102 / 255
//        detVC.view.backgroundColor = UIColor(red: color, green: color, blue: color, alpha: 1)
//       detVC.groupTitleLabel.text = UserDefaults.standard.string(forKey: "currentAuditoriaBodegaGroupId")
//       detVC.update(idDetail: "")
        
      
//        // CON SERVICIOS let currID =  UserDefaults.standard.string(forKey: "currentAuditoriaBodegaGroupId")
//        if  let paso =  UserDefaults.standard.string(forKey: "currentAuditoriaBodegaGroupId") {
//            detVC.update(idDetail: paso)// falta pasar AuditoriaBodegaGroup
//        }else{ return}
        
       // UserDefaults.standard.
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone){
            let detVC : DetailGroupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as! DetailGroupViewController
            let color : CGFloat = 102 / 255
            detVC.view.backgroundColor = UIColor(red: color, green: color, blue: color, alpha: 1)
            detVC.groupTitleLabel.text = UserDefaults.standard.string(forKey: "currentAuditoriaBodegaGroupId")
            detVC.update(idDetail: "")
            navigationController?.pushViewController(detVC, animated: true)
            
        }else{
            
            
            // add child view controller view to container
            
            let controller = storyboard!.instantiateViewController(withIdentifier: "DetailVC") as! DetailGroupViewController
            addChildViewController(controller)
            controller.view.translatesAutoresizingMaskIntoConstraints = false
            detailContainerView.addSubview(controller.view)
            
            NSLayoutConstraint.activate([
                controller.view.leadingAnchor.constraint(equalTo: detailContainerView.leadingAnchor),
                controller.view.trailingAnchor.constraint(equalTo: detailContainerView.trailingAnchor),
                controller.view.topAnchor.constraint(equalTo: detailContainerView.topAnchor),
                controller.view.bottomAnchor.constraint(equalTo: detailContainerView.bottomAnchor)
                ])
            
            controller.didMove(toParentViewController: self)
            controller.groupTitleLabel.text = UserDefaults.standard.string(forKey: "currentAuditoriaBodegaGroupId")
            

            detailContainerView.alpha = 1
            shape.alpha = 0.7
        }
        
        }
   
    @objc func hideDetailAuditoriaAction(notification: NSNotification) {
            detailContainerView.alpha = 0
            shape.alpha = 0
    }
    }

//func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    if segue.identifier == "Pepe" {
//        //containerViewController = segue.destinationViewController as? YourContainerViewControllerClass
//    }
//}
//


