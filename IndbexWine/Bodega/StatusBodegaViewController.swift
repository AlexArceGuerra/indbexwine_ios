//
//  StatusBodegaViewController.swift
//  IndbexWine
//
//  Created by Mac-27 on 6/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class StatusBodegaViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
  
    @IBOutlet weak var bodegasStatusCW: UICollectionView!
    
    let reuseIdentifier = "Cell" // also enter this string as the cell identifier in the storyboard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bodegasStatusCW.delegate = self
        bodegasStatusCW.dataSource = self
        statusBodegaLoad()
        
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
      
        
        
        bodegasStatusCW.collectionViewLayout.invalidateLayout()
        self.view.setNeedsDisplay()
        bodegasStatusCW.reloadData()
    }
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  statusBodegaArray.count // self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! StatusBodegaCollectionViewCell
        
        let myStatusBodega = statusBodegaArray [indexPath.row]
        
        cell.update(statusBodega: myStatusBodega)
        return cell
    }
    
    
    
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        
        
        // This is Just for example , for the scenario Step-I -> 1
        let yourWidthOfLable = (bodegasStatusCW.frame.size.width/3) - 10
        //let font = UIFont(name: "Helvetica", size: 20.0)
        
        //var expectedHeight = heightForLable(array[indePath.row], font: font, width:yourWidthOfLable )
        let expectedHeight = bodegasStatusCW.frame.size.height
        
        return CGSize(width: yourWidthOfLable, height: expectedHeight)
    }
    
    func statusBodegaLoad()  {
        
        statusBodegaArray.removeAll()
        
        var statusBodega = StatusBodega.init(title: "Mercado", percent: "24")
        statusBodegaArray.append(statusBodega)
          statusBodega = StatusBodega.init(title: "Auditoria", percent: "43")
        statusBodegaArray.append(statusBodega)
          statusBodega = StatusBodega.init(title: "Caducidad", percent: "78")
        statusBodegaArray.append(statusBodega)

        
    }
    
    
    
    
}


