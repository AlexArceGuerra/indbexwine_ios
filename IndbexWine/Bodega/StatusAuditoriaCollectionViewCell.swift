//
//  StatusAuditoriaCollectionViewCell.swift
//  IndbexWine
//
//  Created by Alejandro Arce on 9/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class StatusAuditoriaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var percentProgressBar: UIProgressView!
  
    
    
    public func update(auditoriaBodegaGroup: AuditoriaBodegaGroup){
        

        titleLabel.text = auditoriaBodegaGroup.title
        
        percentProgressBar.progress = Float(auditoriaBodegaGroup.percent)! / 100
        percentProgressBar.tintColor = UIColor(cgColor: statusColor(progress: Double(auditoriaBodegaGroup.percent)))
        
        
    }
    
//    @IBAction func showDetailAction(_ sender: Any) {
//
//
////        let my
//        
//
//
//
//        // notificacion a BodegaViewController
//        NotificationCenter.default.post(name: .showDetailAuditoria, object: nil)
//
//
//
//    }
}
