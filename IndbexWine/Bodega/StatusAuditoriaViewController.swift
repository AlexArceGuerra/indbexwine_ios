//
//  StatusAuditoriaViewController.swift
//  IndbexWine
//
//  Created by Alejandro Arce on 9/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class StatusAuditoriaViewController:  UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var statusAuditoriaCW: UICollectionView!
    
    
    let reuseIdentifier = "Cell2" // also enter this string as the cell identifier in the storyboard

    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusAuditoriaCW.delegate = self
        statusAuditoriaCW.dataSource = self
        AuditoriaBodegaGroupLoad()
        
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        statusAuditoriaCW.collectionViewLayout.invalidateLayout()
        self.view.setNeedsDisplay()
    }
    
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  auditoriaBodegaGroupArray.count // self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! StatusAuditoriaCollectionViewCell
        
//        cell.update()
//        cell.titleLabel.text = items[indexPath.row]
        
        cell.update(auditoriaBodegaGroup: auditoriaBodegaGroupArray[indexPath.row])
        return cell
    }
    
    
    
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        // pass params
        // CON SERVICIOS    UserDefaults.standard.set( auditoriaBodegaGroupArray[indexPath.row].id, forKey: "currentAuditoriaBodegaGroupId")
        UserDefaults.standard.set( auditoriaBodegaGroupArray[indexPath.row].title, forKey: "currentAuditoriaBodegaGroupId")
        // Notification to BodebaViewController
        NotificationCenter.default.post(name: .showDetailAuditoria, object: nil)
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var numCol : CGFloat = 4
        var numRow : CGFloat = 6
        
        
        
        if (UIDevice.current.orientation == UIDeviceOrientation.portrait ||   UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown){
            numCol = 3
            numRow = 8
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            numCol = 3
            numRow = 8
        }
        
        let yourWidthOfLable = (statusAuditoriaCW.frame.size.width / numCol)-1
        let expectedHeight = (statusAuditoriaCW.frame.size.height / numRow)
        
        return CGSize(width: yourWidthOfLable, height: expectedHeight)
    }
    
    
    func AuditoriaBodegaGroupLoad()  {
        
        auditoriaBodegaGroupArray.removeAll()

        var auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "0", title: "General", percent: "24", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "1", title: "Sistema de calidad", percent: "30", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "2", title: "Laboratorio", percent: "12", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "3", title: "Control volumen", percent: "43", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "4", title: "Enbotellado", percent: "65", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "5", title: "Limpieza y esterielización", percent: "71", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "6", title: "Filtrado vino", percent: "5", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "7", title: "Contaminación", percent: "28", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "8", title: "Sellado embotellado", percent: "25", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "9", title: "Tapones", percent: "18", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "10", title: "Lote mercado", percent: "30", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "11", title: "Etiquetas", percent: "42", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "12", title: "Empaquetado", percent: "65", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "13", title: "Denominación ", percent: "12", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "14", title: "Viñedos", percent: "23", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "15", title: "Monitorización", percent: "83", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "16", title: "Producción", percent: "65", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "17", title: "Prensado", percent: "12", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "18", title: "Fermentación", percent: "75", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "19", title: "Envejecimiento", percent: "70", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "20", title: "Compra vinos", percent: "16", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "21", title: "Vinos espumosos", percent: "7", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "22", title: "Producción orgánica", percent: "65", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        auditoriaBodegaGroup = AuditoriaBodegaGroup.init(id: "23", title: "Transporte", percent: "90", orden: "1")
        auditoriaBodegaGroupArray.append(auditoriaBodegaGroup)
        
        
    }
    
    
    
}


