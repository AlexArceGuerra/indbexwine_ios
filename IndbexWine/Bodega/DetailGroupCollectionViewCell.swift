//
//  DetailGroupCollectionViewCell.swift
//  IndbexWine
//
//  Created by Mac-27 on 11/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class DetailGroupCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    
    
    public func update(detailGroup : DetailGroup){
        //var frame = self.frame
        
        titleLabel.text = detailGroup.title
        valueTextField.text = detailGroup.value
        setColorTextField(textField:valueTextField)
        
        
    }

    
    private func setColorTextField(textField: UITextField){
        
        if (textField.text == ""){
            
            //textField.backgroundColor = UIColor.red
            textField.backgroundColor = UIColor(displayP3Red: 1, green: 102/255, blue: 102/255, alpha: 1)
        }
        
        
    }
    
}
