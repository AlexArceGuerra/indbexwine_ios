//
//  DetailGroupViewController.swift
//  IndbexWine
//
//  Created by Mac-27 on 11/10/17.
//  Copyright © 2017 Mac-27. All rights reserved.
//

import UIKit

class DetailGroupViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
   

    let reuseIdentifier = "Cell3" // also enter this string as the cell identifier in the storyboard
    @IBOutlet weak var groupTitleLabel: UILabel!
    @IBOutlet weak var detailGroupCW: UICollectionView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailGroupCW.delegate = self
        detailGroupCW.dataSource = self
          detailGroupLoad()
        // toDelete.text = "1234566789"
    }

    @IBAction func backAction(_ sender: Any) {
        NotificationCenter.default.post(name: .hideDetailAuditoria, object: nil)
       navigationController?.popViewController(animated: true)
    }
    
     func update(idDetail : String!){
       
     detailGroupLoad()
 
        // CON SERVICIOS  BUSCA CON ID
       // groupTitleLabel.text = "pepe"
        
//        if let paso  = idDetail{
//            groupTitleLabel.text = paso
//            detailGroupLoad()}else{return}
        
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        detailGroupCW.collectionViewLayout.invalidateLayout()
        self.view.setNeedsDisplay()
    }

    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return detailGroupArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! DetailGroupCollectionViewCell
        
        //        cell.update()
        //        cell.titleLabel.text = items[indexPath.row]
        
        cell.update(detailGroup: detailGroupArray[indexPath.row])
        return cell
    }
    
    
    
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let yourWidthOfLable = (detailGroupCW.frame.size.width)-1
       // let items = detailGroupCW.frame.size.height/20
       // let expectedHeight = (detailGroupCW.frame.size.height/items)
        let expectedHeight = CGFloat(30)
        return CGSize(width: yourWidthOfLable, height: expectedHeight)
    }
    
    
    func detailGroupLoad()  {
        
        detailGroupArray.removeAll()
        var detailGroup = DetailGroup.init(ordenFromGroup: "1", orden: "1", title: "how are the filters and filler cleaned and sterilised before bottling commences?", list: "", value: "valor uno", weight: "20")
        detailGroupArray.append(detailGroup)
        detailGroup = DetailGroup.init(ordenFromGroup: "2", orden: "1", title: "filter type", list: "", value: "valor uno", weight: "20")
        detailGroupArray.append(detailGroup)
        detailGroup = DetailGroup.init(ordenFromGroup: "3", orden: "1", title: "manufacturer of filter", list: "", value: "", weight: "20")
        detailGroupArray.append(detailGroup)
        detailGroup = DetailGroup.init(ordenFromGroup: "3", orden: "1", title: "bottling line", list: "", value: "", weight: "20")
        detailGroupArray.append(detailGroup)
        detailGroup = DetailGroup.init(ordenFromGroup: "3", orden: "1", title: "if mobile or contract bottler used, name of society used", list: "", value: "valor uno", weight: "20")
        detailGroupArray.append(detailGroup)
        detailGroup = DetailGroup.init(ordenFromGroup: "3", orden: "1", title: "how long are bottles stored on site?:", list: "", value: "", weight: "20")
        detailGroupArray.append(detailGroup)

}
}
